# nerd-fonts

This repository contains necessary files for building your own Nerd-fonts.

## Why?

[Nerd fonts repository](https://github.com/ryanoasis/nerd-fonts) is 6GB in size, which is a pain to clone if all you need is patching your own fonts.

## Usage

You need `fontforge` and `fonttools` installed. Clone this repository, then:

```bash
cd nerd-fonts

# Update the files if needed (need `curl` and 'bash')
./update.sh

# Patch your font
fontforge -script font-patcher --careful --complete path-to-your-original-font.ttf
```

## License

- The python scripts are taken from <https://github.com/jonz94/Sarasa-Gothic-Nerd-Fonts>
- The files downloaded with `update.sh` fall under their original licenses
- Other are MIT
