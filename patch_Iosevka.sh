#!/usr/bin/env bash

# usage: patch_Iosevka.sh [version of Iosevka]

set -eu

if ! command -v fontforge >/dev/null; then
    printf "\033[1;31mfontforge\033[0m is not installed.\n"
    exit 1
fi

if [[ $# -eq 0 ]]; then
    versions=$(curl -H "Accept: application/vnd.github.v3+json" -s https://api.github.com/repos/be5invis/Iosevka/releases | jq -r '.[] | .tag_name' | sed -e 's/^v//g')
    version=$(echo "${versions}" | fzf --no-multi --prompt "Release: ")
else
    version="$1"
fi

variants=(
  "bold"
  "italic"
  "bolditalic"
  "regular"
)

# Get the zip files from Github
zipfile="original/ttf-iosevka-term-${version}.zip"
if [ ! -f "${zipfile}" ]; then
    printf "\033[1;34mDownloading Iosevka Term version \033[1;31m%s\033[1;34m zip file ...\033[0m\n" "${version}"
    curl -fSL "https://github.com/be5invis/Iosevka/releases/download/v${version}/ttf-iosevka-term-${version}.zip" -o "${zipfile}"
fi

printf "\033[1;34mUnzipping the downloaded archive ...\033[0m\n"
unzip "${zipfile}" -d ./original

for variant in "${variants[@]}"; do
    printf "\033[1;34mPatching Iosevka term \033[1;31m%s\033[1;34m ...\033[0m\n" "${variant}"

    # Run the font-patcher script
    fontforge -script ./font-patcher --careful --complete ./original/"iosevka-term-${variant}.ttf"
    mv -fv ./IosevkaTermNerdFont-*.ttf ./patched/"iosevka-term-${variant}-nerd-font.ttf"
done
