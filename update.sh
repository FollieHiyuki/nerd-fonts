#!/usr/bin/env bash

echo "Downloading 'font-patcher' script..."
curl -fsSL https://github.com/ryanoasis/nerd-fonts/raw/master/font-patcher -o ./font-patcher
chmod 755 ./font-patcher

for path in codicons font-awesome materialdesign octicons powerline-symbols weather-icons; do
    mkdir -p ./src/glyphs/"$path"
done
mkdir -p ./bin/scripts/name_parser

glyphs=(
  "codicons/codicon.ttf"
  "font-awesome/FontAwesome.otf"
  "materialdesign/MaterialDesignIconsDesktop.ttf"
  "materialdesign/MaterialDesignIconsDesktop_orig.ttf"
  "octicons/octicons.ttf"
  "powerline-symbols/PowerlineSymbols.otf"
  "weather-icons/weathericons-regular-webfont.ttf"
  "Pomicons.otf"
  "PowerlineExtraSymbols.otf"
  "Unicode_IEC_symbol_font.otf"
  "devicons.ttf"
  "extraglyphs.sfd"
  "font-awesome-extension.ttf"
  "font-logos.ttf"
  "materialdesignicons-webfont.ttf"
  "original-source.otf"
)

name_parser=(
  "FontnameParser.py"
  "FontnameTools.py"
  "query_monospace"
  "query_names"
  "query_panose"
  "query_sftn"
  "query_version"
)

upstream_src_glyphs_url="https://github.com/ryanoasis/nerd-fonts/raw/master/src/glyphs"
upstream_name_parser_url="https://github.com/ryanoasis/nerd-fonts/raw/master/bin/scripts/name_parser"

echo "Downloading glyph fonts..."
for glyph in "${glyphs[@]}"; do
  # replace all `whitespace` characters with `%20`
  percent_encoded_uri="${upstream_src_glyphs_url}/${glyph//\ /%20}"

  curl -fSL "${percent_encoded_uri}" -o "src/glyphs/${glyph}"
done
find ./src/glyphs/ -type f -exec chmod 644 '{}' \;

echo "Downloading helper scripts for font-patcher ..."
for file in "${name_parser[@]}"; do
  curl -fSL "${upstream_name_parser_url}/${file}" -o "bin/scripts/name_parser/${file}"
done
find ./bin/scripts/name_parser/ -type f -exec chmod 644 '{}' \;
